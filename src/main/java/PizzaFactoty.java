public class PizzaFactoty {
    public Pizza createPizza(String pizza){
        PizzaBuilder pizzaBuilder = PizzaBuilder.getBuilderForPizza(pizza)
            .bakeCake()
            .cutIngredients(Ingredients.MOZZARELLA)
            .addIngredient(Ingredients.MOZZARELLA)
            .bake(pizza)
            .box(pizza)
            .deliver(pizza);
        return pizzaBuilder;
    }
}

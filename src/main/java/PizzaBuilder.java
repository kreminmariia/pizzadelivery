public class PizzaBuilder {
    private Pizza pizzaToBake;

    private PizzaBuilder(Pizza pizza) {
        pizzaToBake = pizza;
    }

    public static PizzaBuilder getBuilderForPizza(String pizzaType) {
        Pizza pizza = null;
        if (pizzaType.equalsIgnoreCase("Margarita")) {
            pizza = new Margarita();
        }
        return new PizzaBuilder(pizza);
    }

    PizzaBuilder bakeCake(PizzaBuilder cake) {
        //info that cake is baked
        return cake;
    }

    public PizzaBuilder cutIngredients(Ingredients ingredient) {
        pizzaToBake.cutIngredient(ingredient);
        //info that ingredients is cut
        return this;
    }

    public PizzaBuilder addIngredient(Ingredients ingredient) {
        pizzaToBake.addIngredient(ingredient);
        //info that ingredients is added
        return this;
    }

    PizzaBuilder bake(PizzaBuilder pizzaWithIngredients) {
        //info that pizza is baked
        return pizzaWithIngredients;
    }

    PizzaBuilder box(PizzaBuilder boxedPizza) {
        //info that pizza is boxed
        return boxedPizza;
    }

    PizzaBuilder deliver(PizzaBuilder pizzaToBake) {
        return pizzaToBake;
    }

}

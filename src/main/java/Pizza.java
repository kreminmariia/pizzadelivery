abstract class Pizza {
    protected List<Ingredients> ingredients;
    public Pizza(){
        ingredients=new LinkedList<Ingredients>();
    }

    public void addIngredient(Ingredients ingredient){
        ingredients.add(ingredient);
    }
    public void cutIngredient(Ingredients ingredient){
        ingredients.add(ingredient);
    }
}
